import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  img;
  constructor(private dataservice: DataService) { }
  public productObj;
  public number: number = 0;

  ngOnInit(): void {
    this.productObj = this.dataservice.item;
  }

  public addToCart(){
      this.number = this.number +1;
  }

}
