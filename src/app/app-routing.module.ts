import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { StartpageComponent } from './startpage/startpage.component';


const routes: Routes = [
    {path: '', component: StartpageComponent},
    {path: 'cart/:id', component: CartComponent},
    // {path: '', component: StartpageComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
// children: [{ 
//   path: 'cart/:id', component: CartComponent 
// }
// ],