import { Component,  } from '@angular/core';
import { MockService } from './mock.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   products = [];
   
  constructor(private mock: MockService) {}

  ngOnInit(){
    this.products = this.mock.items;
    
    
    
  }

 
}
