import { Component, OnInit } from '@angular/core';
import { MockService } from '../mock.service';
import { DataService } from '../data.service';


@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent implements OnInit {
products = [];
public itemObj: {};
  constructor(private mock: MockService, private dataservice: DataService) { }

  ngOnInit(): void {
    this.products = this.mock.items;
  }

  public storeToService(product) {
    const url = `/cart/${product.id}`;
    
    this.dataservice.productItem(product);
      
  }

}
